const express = require('express');
const router = express.Router();
const managerRouter = require('./manager.route');
const golfRouter = require('./golf.route');

router.use(managerRouter);
router.use(golfRouter);

module.exports = router;