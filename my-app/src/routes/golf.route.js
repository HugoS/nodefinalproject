const express = require('express');
const router = express.Router();
const golf = require('../controllers/golf.controller');
// const verifyToken = require('../helpers/verifyToken');

// create a new manager
router.post('/golfs', golf.create);
router.delete('/golf/cleardb', golf.deleteAll);
router.get('/golfs', /*verifyToken,*/ golf.findAll);
router.get('/golfs/:id', /*verifyToken,*/ golf.findById);
router.post('/golfs/:id/', golf.findAndUpdate);
// router.put('/users/:id/', user.findAndUpdate);
router.delete('/golfs/:id/', golf.deleteOne);


module.exports =router;