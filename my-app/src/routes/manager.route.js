const express = require('express');
const router = express.Router();
const manager = require('../controllers/manager.controller');
// const verifyToken = require('../helpers/verifyToken');

// create a new manager
router.post('/managers', manager.create);
router.delete('/manager/cleardb', manager.deleteAll);
router.get('/managers', /*verifyToken,*/ manager.findAll);
router.get('/managers/:id', /*verifyToken,*/ manager.findById);
router.post('/managers/:id/', manager.findAndUpdate);
// router.put('/users/:id/', user.findAndUpdate);
router.delete('/managers/:id/', manager.deleteOne);


module.exports =router;
