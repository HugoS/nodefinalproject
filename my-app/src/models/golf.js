const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const golfSchema = new Schema({
    id: {
        type: Number,
        required: true,
    },
    title: {
        type: String
    },
    latitude: {
        type: Number
    },
    longitude: {
        type: Number
    },
    description: {
        type: String
    },
    manager: {
        type: String
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('Golf', golfSchema)