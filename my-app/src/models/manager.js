const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const managerSchema = new Schema({
    id: {
        type: Number,
        required: true,
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    mail: {
        type: String
    },
    telephone: {
        type: Number
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('Manager', managerSchema)